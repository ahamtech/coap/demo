defmodule Coapdemo do
use Application
  
  def start() do
    import Supervisor.Spec
    children = [
      supervisor(Aadya.Server,[Coapdemo.Router],[])
    ]
    opts = [strategy: :one_for_all, name: Coapdemo.Supervisor]
    Supervisor.start_link(children, opts)
    end
end
