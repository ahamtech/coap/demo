defmodule Hello do
  def hello path do
    case :ets.lookup :resource,path do
      [] ->
        { {:error, :not_found}, :nil }
      [{_,data}] ->
        { :created , data }
      end
  end
  def post path,data do
    case :ets.insert(:resource,{path,data}) do
      true ->
        {:ok, data}
      _ ->
        IO.puts "something went worong"
        {:error}
    end
  end
  def put path,data do
    case :ets.insert(:resource,{path,data}) do
      true ->
        {:ok, data}
      _ ->
        IO.puts "something went worong"
        {:error}
    end
  end
end
