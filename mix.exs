defmodule Coapdemo.Mixfile do
  use Mix.Project

  def project do
    [app: :coapdemo,
     version: "0.1.1",
     elixir: "~> 1.6",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [
    applications: [:logger,:poolboy, :phoenix, :phoenix_html, :cowboy]
  ]# Specify extra applications you'll use from Erlang/Elixir
  end

  defp deps do
    [
     {:aadya, "~> 0.1.0"},
     {:poolboy, "~> 1.5.1"},
     {:logger_papertrail_backend, "~> 0.2.1"},
     {:credo, "~> 0.6.1", only: [:dev, :test]},
     {:phoenix, "~> 1.3.0"},
     {:phoenix_html, "~> 2.6"},
     {:ex_doc, "~> 0.14", only: :dev, runtime: false},
     {:cowboy, "~> 1.0"},
     {:gelf_logger, "~> 0.7.3"}
    ]
  end
end
