# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :aadya, web: true

config :aadya, Aadya.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "6wUbtNztAqYI/Bn1o2UZ5+BqC4q4PzptFiO3jYMrFOc47wY21hCk4gfdhuqG6Rwd",
  render_errors: [view: Aadya.ErrorView, accepts: ~w(html json)],
  http: [port: 4000],
  debug_errors: true,
  web: true

config :logger, :gelf_logger,
      port: 2202,
      application: "aadya",
      compression: :raw,
      metadata: [:request_id, :function, :module, :file, :line],
      host: "gra2.logs.ovh.com",
      tags: [
         "X-OVH-TOKEN": "28d6dac4-7668-4c6c-b360-24b5c98a674b"
      ]

config :logger,
  backends: [:console, {Logger.Backends.Gelf, :gelf_logger}],
  level: :debug
